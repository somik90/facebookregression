package com.facebook.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class FacebookMethodRepository {
	static WebDriver driver;

	// App launch method via Google Chrome
	void chromeLaunch() {

		// For disable notification new comment
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		// End of disable notification
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver(options); // pass option to disable notification
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
	}

	// App launch method via Mozilla Firefox
	void firefoxLaunch() {
		// For new version of Mozilla Firefox after 47.0
		System.setProperty("webdriver.firefox.marionette", "./firefoxdriver/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("https://www.facebook.com/");
		// String baseUrl = "https://www.facebook.com/";
		// driver.get(baseUrl);
	}

	// Login and perform action via Robot class
	void verifyRobotLogin() throws AWTException {
		// Find and click username field
		driver.findElement(By.xpath("//input[@name='email']")).click();

		// Enter letters one by one through Robot class
		/*
		 * Robot btntab = new Robot(); btntab.keyPress(KeyEvent.VK_S);
		 * btntab.keyRelease(KeyEvent.VK_S); btntab.keyPress(KeyEvent.VK_O);
		 * btntab.keyRelease(KeyEvent.VK_O);
		 */

		// Enter full string(username or email) through robot class
		String text1 = "cftester8@gmail.com";
		// Copy the string to the system clipboard
		StringSelection stringSelection1 = new StringSelection(text1);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection1, stringSelection1);
		// Paste the string through robot class
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		// Find and click password field
		driver.findElement(By.xpath("//input[@name='pass']")).click();
		// Enter full string(Password) through robot class
		String text2 = "cf12345678";
		// Copy the string to the system clipboard
		StringSelection stringSelection2 = new StringSelection(text2);
		Clipboard clipboard2 = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard2.setContents(stringSelection2, stringSelection2);
		// Paste the string through robot class
		Robot robot2 = new Robot();
		robot2.keyPress(KeyEvent.VK_CONTROL);
		robot2.keyPress(KeyEvent.VK_V);
		robot2.keyRelease(KeyEvent.VK_V);
		robot2.keyRelease(KeyEvent.VK_CONTROL);

		// Click on Login button
		Robot clickLogin = new Robot();
		clickLogin.keyPress(KeyEvent.VK_ENTER);
		clickLogin.keyRelease(KeyEvent.VK_ENTER);
	}

	// Normal login via Xpath, ID, Linktext
	void verifyLogin() {
		WebElement uname = driver.findElement(By.xpath("//input[@name='email']"));
		uname.sendKeys("cftester8@gmail.com");
		WebElement pass = driver.findElement(By.id("pass"));
		pass.sendKeys("cf12345678");
		WebElement login = driver.findElement(By.id("u_0_2"));
		login.click();
	}

	// AutoIT Script for file upload
	void imageUpload() throws InterruptedException, IOException {
		// WebElement myprofile = driver.findElement(By.linkText("Profile"));
		WebElement myprofile = driver.findElement(By.xpath(".//*[@id='u_0_a']/div[1]/div[1]/div/a"));
		myprofile.click();
		Thread.sleep(3000);
		WebElement photos = driver.findElement(By.linkText("Photos"));
		photos.click();
		Thread.sleep(3000);
		WebElement btnaddphoto = driver.findElement(By.linkText("Add Photos/Video"));
		btnaddphoto.click();
		Thread.sleep(3000);

		// AutoIT Script for image upload
		Runtime.getRuntime().exec("./AutoITScript/facebookuploadnew.exe");
		Thread.sleep(3000);
		WebElement btnpostphoto = driver.findElement(By.linkText("Post"));
		btnpostphoto.click();
	}

	// AutoIT Script for windows authentication
	void handleWindowsAuthentication() throws IOException {
		// For Chrome authentication
		/*
		 * System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 * driver = new ChromeDriver(); driver.manage().window().maximize();
		 * driver.get("http://www.engprod-charter.net");
		 * Runtime.getRuntime().exec("./AutoITScript/windowsauthentication.exe");
		 */

		// For firefox authentication
		System.setProperty("webdriver.firefox.marionette", "./firefoxdriver/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://www.engprod-charter.net");
		Runtime.getRuntime().exec("./AutoITScript/handleauthentication.exe");
	}

	// Login and perform action via JavaExecutor code
	void performJavaExecutor() throws InterruptedException {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		// Creating the JavascriptExecutor interface object by Type casting
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		Thread.sleep(5000);
		// Vertical scroll down by 400 pixels
		js.executeScript("window.scrollBy(0, 400)");
		Thread.sleep(6000);
		js.executeScript("window.scrollBy(0, -400)");
		// for scrolling till the bottom of the page we can use the code like
		// js.executeScript("window.scrollBy(0,document.body.scrollHeight)");

		// Enter value in Username and Password
		js.executeScript("document.getElementById('email').value='cftester8@gmail.com'");
		js.executeScript("document.getElementById('pass').value='cf12345678'");
		// Click on Login button
		// WebElement login = driver.findElement(By.id("u_0_2"));
		// Thread.sleep(3000);
		// js.executeScript("arguments[0].click();", login);
		Thread.sleep(3000);
		js.executeScript("document.getElementById('u_0_2').click();");

		// to refresh browser window using Javascript
		js.executeScript("history.go(0)");

		// to get the Title of our webpage String sText =
		String sText2 = js.executeScript("return document.title;").toString();
		System.out.println(sText2);

		// to get the domain String sText =
		String sText3 = js.executeScript("return document.domain;").toString();
		System.out.println(sText3);

		// to get the URL of our webpage String sText =
		String sText4 = js.executeScript("return document.URL;").toString();
		System.out.println(sText4);

		// to get innertext of the entire webpage in Selenium String sText =
		String sText1 = js.executeScript("return document.documentElement.innerText;").toString();
		System.out.println(sText1);

		// to handle checkbox
		// js.executeScript("document.getElementById('enter element
		// id').checked=false;");

		// to generate Alert Pop window in selenium
		// js.executeScript("alert('Welcome to the world of FACEBOOK');");

		/*
		 * // to click on a SubMenu which is only visible on mouse hover on Menu?
		 * //Hover on Automation Menu on the MenuBar js.
		 * executeScript("$('ul.menus.menu-secondary.sf-js-enabled.sub-menu li').hover()"
		 * );
		 */

		/*
		 * //to navigate to different page using Javascript? //Navigate to new Page
		 * js.executeScript("window.location = 'https://www.softwaretestingmaterial.com"
		 * );
		 */
	}

	// Login and perform action via Action class
	void performActionClass() throws InterruptedException {
		// For disable notification new comment
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		// End of disable notification
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver(options); // pass option to disable notification
		driver.manage().window().maximize();
		driver.get("https://www.snapdeal.com/");
		// Create object 'action' of an Actions class
		Actions actions = new Actions(driver);
		// Main Menu
		WebElement mainMenu = driver
				.findElement(By.xpath(".//*[@id='leftNavMenuRevamp']/div[1]/div[2]/ul/li[7]/a/span"));
		actions.moveToElement(mainMenu);
		// actions.click().build().perform();
		// Sub Menu
		WebElement subMenu = driver.findElement(By.xpath(".//*[@id='category6Data']/div[1]/div/div/p[2]/a/span"));
		// To mouseover on sub menu
		actions.moveToElement(subMenu);
		// build() method is used to compile all the actions into a single step
		actions.click().build().perform();
		WebElement rc = driver.findElement(By.id("642057816655"));
		Action rightclick = actions.contextClick(rc).build();
		rightclick.perform();
		actions.contextClick(rc).sendKeys(Keys.ARROW_DOWN);
		actions.contextClick(rc).sendKeys(Keys.ENTER);
		actions.contextClick(rc).build().perform();
		actions.contextClick(rc).release();

		// actions.contextClick(rc).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		// actions.contextClick(rc).release();
		Set<String> winid = driver.getWindowHandles();
		Iterator<String> iter = winid.iterator();
		iter.next();
		String tab = iter.next();
		driver.switchTo().window(tab);
		System.out.println(driver.getTitle());
	}

	// Login and perform action via Sikuli
	void sikuli() throws FindFailed {
		// Creating Object of 'Screen' class
		// Screen is a base class provided by Sikuli. It allows us to access all the
		// methods provided by Sikuli.
		Screen screen = new Screen();
		// Creating Object of Pattern class and specify the path of specified images
		// I have captured images of Facebook Email id field, Password field and Login
		// button and placed in my project directory sikuliimage
		// Facebook user id image
		Pattern image1 = new Pattern("./sikuliimage/auname.jpg");
		// Facebook password image
		Pattern image2 = new Pattern("./sikuliimage/bpass.jpg");
		// Facebook login button image
		Pattern image3 = new Pattern("./sikuliimage/login.jpg");
		// Initialization of driver object to launch firefox browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		screen.wait(image1, 10);
		// Calling 'type' method to enter username in the email field using 'screen'
		// object
		screen.type(image1, "cftester8@gmail.com");
		// Calling the same 'type' method and passing text in the password field
		screen.type(image2, "cf12345678");
		// This will click on login button
		screen.click(image3);
	}
}