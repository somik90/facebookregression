package com.facebook.test;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

public class MainDriver {

	public static void main(String[] args) throws AWTException, InterruptedException, IOException, FindFailed {
		// TODO Auto-generated method stub
		FacebookMethodRepository fmd = new FacebookMethodRepository();
		//fmd.chromeLaunch();
		//fmd.firefoxLaunch();
		//fmd.verifyLogin();
		//fmd.verifyRobotLogin();
		//fmd.imageUpload();
		//fmd.handleWindowsAuthentication();
		//fmd.performJavaExecutor();
		//fmd.performActionClass();
		fmd.sikuli();
	}
}
